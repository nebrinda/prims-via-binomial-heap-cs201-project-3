//written by nathan brinda
#include <stdio.h>
#include <stdlib.h>
#include "CDLL.h"

CDLL* newCDLL(){
   CDLL* temp = malloc(sizeof(CDLL));
   if (temp == NULL){
      fprintf(stderr, "out of memory");
      exit(1);
   }
   temp->size = 0;
   temp->head = NULL;
   temp->tail = NULL;
   return temp;   
}

Node* insertForCDLL(CDLL* list, Node* value_to_add){
   
   if (list->size == 0){
      list->head = value_to_add;
      list->tail = value_to_add;
      value_to_add->next = list->tail;
      value_to_add->prev = list->head;
   }
   else{
      value_to_add->prev = list->tail;
      list->tail->next = value_to_add;
      value_to_add->next = list->head;
      list->tail = value_to_add;
      list->tail->next = list->head;
      list->head->prev = list->tail;
   }
      list->size++;
   return value_to_add;
}

void deleteForCDLL(CDLL* list, Node* value_to_be_deleted){

      value_to_be_deleted->next->prev = value_to_be_deleted->prev;//don't need a check for NULL since it is circular.
      value_to_be_deleted->prev->next = value_to_be_deleted->next;
      
      if (value_to_be_deleted == list->head){     
      list->head = value_to_be_deleted->next;
   }
   
   if (value_to_be_deleted == list->tail){
      list->tail = value_to_be_deleted->prev;
   }
   
   list->size--;

}


//returns a list with the donor added on to the end of the recipient
CDLL* merge(CDLL* recipient, CDLL* donor){
   
   if (donor->size == 0)
      return recipient;

   if (recipient->size == 0){
      recipient->head = donor->head;
      recipient->tail = donor->tail;
      recipient->size = donor->size;
      return recipient;
   }
      
   recipient->tail->next = donor->head;
   recipient->tail->next->prev = recipient->tail;
   recipient->tail = donor->tail;
   recipient->tail->next = recipient->head;
   recipient->head->prev = recipient->tail;
   recipient->size += donor->size;
   return recipient;
}


