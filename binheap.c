//written by nathan brinda
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "binheap.h"
#include "Vertex.h"

static Node* bubbleUp(binheap* bin_heap, Node* node);
static Node* combine(binheap* bin_heap, Node*x, Node* y);
static void updateConsolidationArray(binheap* bin_heap, Node** D, Node* node);
static void consolidate(binheap* bin_heap);
static void updateParentPointers(CDLL* list);

binheap* newBinHeap(int(*c)(void*,void*),void(*u)(void*,Node*)){
   binheap* bheap = malloc(sizeof(binheap));
   if (bheap == NULL){
      fprintf(stderr, "out of memory");
      exit(1);  
   }
   bheap->size = 0;
   bheap->root_list = newCDLL();;
   bheap->extreme = NULL;
   bheap->comparator = c;  
   bheap->updater = u;
   return bheap;
}

Node* insertBinHeap(binheap* bin_heap, void* value){

   Node* node = newNode();
   node->value = value;
   node->parent = node;
   node->child = newCDLL();
   insertForCDLL(bin_heap->root_list, node);
   bin_heap->size++;
   consolidate(bin_heap);
   return node;
}

Node* decreaseKeyBinHeap(binheap* bin_heap, Node* node, void* value){
   node->value = value;
   node = bubbleUp(bin_heap, node);
   if (bin_heap->comparator(node->value, bin_heap->extreme->value) < 0)
      bin_heap->extreme = node;
   return node;
}

Node* extractBinHeap(binheap* bin_heap){

   Node* extreme_value = bin_heap->extreme;

   if (bin_heap->size == 7){
      printf("the root list is: %d %d %d\n", ((Vertex*)bin_heap->root_list->head->value)->vertex_value, ((Vertex*)bin_heap->root_list->head->next->value)->vertex_value, ((Vertex*)bin_heap->root_list->head->next->next->value)->vertex_value);
      printf("the children of 1 are: %d %d\n", ((Vertex*)bin_heap->root_list->head->next->next->child->head->value)->vertex_value, ((Vertex*)bin_heap->root_list->head->next->next->child->head->next->value)->vertex_value);
      printf("the children of 9 are: %d\n", ((Vertex*)bin_heap->root_list->head->next->child->head->value)->vertex_value);
   }
   deleteForCDLL(bin_heap->root_list, extreme_value);
   updateParentPointers(extreme_value->child);
   merge(bin_heap->root_list, extreme_value->child);
   consolidate(bin_heap);
   bin_heap->size--;
   return extreme_value;
}

static void consolidate(binheap* bin_heap){

   int size_of_array = (int)(((log10(bin_heap->size)) / (log10(2))) + .5) + 1;

   Node** D = calloc(size_of_array, sizeof(Node));
   Node* temp;

   while (bin_heap->root_list->size > 0){
      temp = bin_heap->root_list->head;
      deleteForCDLL(bin_heap->root_list, temp);
      updateConsolidationArray(bin_heap, D, temp);
   }

   bin_heap->extreme = NULL;
   for (int i = 0; i < size_of_array; i++){
      
      if (D[i] != NULL){
         insertForCDLL(bin_heap->root_list, D[i]);
         if (bin_heap->extreme == NULL || bin_heap->comparator(D[i]->value, bin_heap->extreme->value) < 0){
            bin_heap->extreme = D[i];
         }
      }
   }
   free(D);
}

static void updateConsolidationArray(binheap* bin_heap, Node** D, Node* node){

   int degree = node->child->size;

   while (D[degree] != NULL){
      node = combine(bin_heap, D[degree],node);
      D[degree] = NULL;
      degree++;
   }
   D[degree] = node;
}


static Node* combine(binheap* bin_heap, Node*x, Node* y){
  
   if (bin_heap->comparator(x->value, y->value) < 0){
      insertForCDLL(x->child, y);
      y->parent = x;
      return x;
   }
   else{
      insertForCDLL(y->child, x);
      x->parent = y;
      return y;
   }
}


static Node* bubbleUp(binheap* bin_heap, Node* node){

   if (node == node->parent) return node;
   if (bin_heap->comparator(node->value, node->parent->value) >= 0) return node;

   bin_heap->updater(node->value, node->parent);
   bin_heap->updater(node->parent->value, node);

   void* temp = node->value;
   node->value = node->parent->value;
   node->parent->value = temp;
   return bubbleUp(bin_heap, node->parent);
}


static void updateParentPointers(CDLL* list){
   
   if (list->size == 0)
      return;
   for(int i = 0; i< list->size; i++){
      list->head->parent = list->head;
      list->head = list->head->next;
   }
   list->head = list->tail->next;
}