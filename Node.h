//written by nathan brinda
#ifndef NODE_H
#define NODE_H
#define EMPTY 0
#define true 1
#define false 0

struct CDLL;

typedef struct Node{

   struct Node* next;
   struct Node* prev;
   struct Node* parent;
   struct CDLL* child;
   void* value;
   
}Node;

Node* newNode();
#endif