//written by nathan brinda
#include <stdlib.h>
#include "Node.h"
#include "CDLL.h"

Node* newNode(){
   Node* node = malloc(sizeof(Node));
   node->value = NULL;
   node->next = NULL;
   node->prev = NULL; 
   node->parent = NULL;
   node->child = newCDLL();
   return node;
}