//written by nathan brinda
#include <stdio.h>
#include <stdlib.h>
#include "Vertex.h"


Vertex* newVertex(int largest_weight)
{
   Vertex* v = malloc(sizeof(Vertex));
   if (v == NULL){
      printf("out of memory");
      exit(1);
   }
   v->owner = NULL;
   v->predecessor = -1;
   v->vertex_value = 0;
   v->key = largest_weight;
   return v;
}



int compare(void* a, void* b){
   Vertex* x = (Vertex*)(a);
   Vertex* y = (Vertex*)(b);
   if ((x->key - y->key) != 0)
      return x->key - y->key;
   if ((x->predecessor - y->predecessor) != 0)
      return x->predecessor - y->predecessor;  
   return x->vertex_value - y->vertex_value;
}

void updateOwner(void* v, Node* n){

   ((Vertex*)v)->owner = n;

}