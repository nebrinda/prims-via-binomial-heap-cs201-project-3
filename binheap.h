//written by nathan brinda
#ifndef BINHEAP_H
#define BINHEAP_H
#include "CDLL.h"

typedef struct binheap{
   int size;
   Node* extreme;
   int (*comparator)(void*,void*);
   void (*updater)(void*, Node*);
   CDLL* root_list;

}binheap;

binheap* newBinHeap(int(*c)(void*, void*),void(*u)(void*,Node*));
Node* insertBinHeap(binheap* bin_heap, void* value);
Node* extractBinHeap(binheap* bin_heap);
Node* decreaseKeyBinHeap(binheap* bin_heap, Node* node, void* value);

#endif