//written by nathan brinda
#ifndef CDLL_H
#define CDLL_h

#include "Node.h"


typedef struct CDLL{

   struct Node* head;
   struct Node* tail;
   int size;   

}CDLL;


CDLL* newCDLL();
Node* insertForCDLL(CDLL* list, Node* value_to_add);
void deleteForCDLL(CDLL* list, Node* value_to_be_deleted);
CDLL* merge(CDLL* recipient, CDLL* donor);

#endif