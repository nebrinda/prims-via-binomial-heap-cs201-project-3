//written by nathan brinda
#ifndef VERTEX_H
#define VERTEX_H
#include "Node.h"


typedef struct Vertex{

   int vertex_value;
   int key;
   int predecessor;
   Node* owner;
}Vertex;

Vertex* newVertex(int largest_weight);
void updateOwner(void* v, Node* n);
int compare(void* a, void* b);
#endif