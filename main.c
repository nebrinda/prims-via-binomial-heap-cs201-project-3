//written by nathan brinda
#include <stdio.h>
#include <stdlib.h>
#include "scanner.h"
#include "binheap.h"
#include "Vertex.h"
#include "Queue.h"
#include "PrintRecord.h"


static void updateVertexKey(binheap* b, Vertex* v, int weight);
static void findLargestInFile(char* filename);
static void initializeGlobals();
static void populateGlobals(char* filename);

void printMST(Vertex* starting_vertex);
void printNode(Vertex* v, int** adjacency_matrix, int* total_weight);
void printList(PrintRecord** list, int size, int depth);
void addList(PrintRecord** list, int *index, Vertex* v, int** adjacency_matrix, int* total_weight);
Vertex* findStartingVertex();
Vertex* Prims();



FILE* fp1;//this is for testing purposes, actual results should be outputted via printf

//globals
int largest_weight;
int largest_vertex;
int** adjacency_matrix;
Vertex** List_of_vertices;
binheap* vertices;


int main(int argc, char** argv){

   fp1 = fopen("output", "a");

   Vertex* starting_vertex;

   findLargestInFile(argv[1]);
   initializeGlobals();
   populateGlobals(argv[1]);

   while (vertices->size > 0){
      starting_vertex = findStartingVertex();
      starting_vertex = Prims(starting_vertex);
      printMST(starting_vertex);
   }

   return  0;
}

Vertex* Prims(Vertex* starting_vertex){

   Vertex* next_vertex = newVertex(largest_weight);
   Vertex** min_spanning_tree = malloc(sizeof(Vertex*) * largest_vertex);
   for (int i = 0; i < largest_vertex; i++)
      min_spanning_tree[i] = 0;

   updateVertexKey(vertices, starting_vertex, 0);
   *next_vertex = *starting_vertex;

   while (vertices->size > 0){
      starting_vertex = extractBinHeap(vertices)->value;
      if (starting_vertex->key == largest_weight){
         insertBinHeap(vertices, starting_vertex);
         break;
      }
      min_spanning_tree[starting_vertex->vertex_value] = starting_vertex;
      for (int i = 0; i < largest_vertex; i++){                                                                                                                
         if (List_of_vertices[i] != NULL && adjacency_matrix[starting_vertex->vertex_value][i] != EMPTY && adjacency_matrix[starting_vertex->vertex_value][i] < List_of_vertices[i]->key && min_spanning_tree[i] == NULL){
            List_of_vertices[i]->key = adjacency_matrix[starting_vertex->vertex_value][i];
            List_of_vertices[i]->predecessor = starting_vertex->vertex_value;
            updateVertexKey(vertices, List_of_vertices[i], List_of_vertices[i]->key);
         }
      }
   }
   return next_vertex;
}


Vertex* findStartingVertex(){

   int smallest = largest_vertex;
   Vertex* start_vertex;
   for (int i = 0; i < largest_vertex; i++){
      if (List_of_vertices[i] != NULL && List_of_vertices[i]->vertex_value < smallest && List_of_vertices[i]->key == largest_weight){//find starting vertex
         start_vertex = List_of_vertices[i];
         smallest = List_of_vertices[i]->vertex_value;
      }
   }
   return start_vertex;
}


static void initializeGlobals(){

   vertices = newBinHeap(compare,updateOwner);

   adjacency_matrix = malloc(sizeof(int*) * (largest_vertex));
   for (int i = 0; i < largest_vertex; i++)
      adjacency_matrix[i] = malloc(sizeof(int) * (largest_vertex));

   for (int i = 0; i < largest_vertex; i++){
      for (int j = 0; j < largest_vertex; j++){
         adjacency_matrix[i][j] = EMPTY;
      }
   }

   List_of_vertices = malloc(sizeof(Vertex*) * largest_vertex);
   for (int i = 0; i < largest_vertex; i++){
      List_of_vertices[i] = NULL;
   }

}

static void populateGlobals(char* filename){

   FILE* fp = fopen(filename, "r");
   int array_index = 0;
   int vertices_and_edge[3];
   for (int i = 0; i < 3; i++)
      vertices_and_edge[i] = EMPTY;

   char* token = readToken(fp);
   while (token)
   {
      if (token[0] != ';'){
         vertices_and_edge[array_index] = atoi(token);
         array_index++;
      }
      else{
         if (vertices_and_edge[2] == EMPTY){
            adjacency_matrix[vertices_and_edge[0]][vertices_and_edge[1]] = 1;
            adjacency_matrix[vertices_and_edge[1]][vertices_and_edge[0]] = 1;
         }
         else if (adjacency_matrix[vertices_and_edge[0]][vertices_and_edge[1]] == EMPTY){
            adjacency_matrix[vertices_and_edge[0]][vertices_and_edge[1]] = vertices_and_edge[2];
            adjacency_matrix[vertices_and_edge[1]][vertices_and_edge[0]] = vertices_and_edge[2];
         }


         Vertex* v1 = newVertex(largest_weight);
         v1->vertex_value = vertices_and_edge[0];
         if (List_of_vertices[v1->vertex_value] == NULL){
            List_of_vertices[v1->vertex_value] = v1;
            v1->owner = insertBinHeap(vertices, v1);
         }

         Vertex* v2 = newVertex(largest_weight);
         v2->vertex_value = vertices_and_edge[1];
         if (List_of_vertices[v2->vertex_value] == NULL){
            List_of_vertices[v2->vertex_value] = v2;
            v2->owner = insertBinHeap(vertices, v2);
         }
         vertices_and_edge[2] = EMPTY;
         array_index = 0;
      }
      token = readToken(fp);
   }
   fclose(fp);
}

static void findLargestInFile(char* filename){

   FILE* fp = fopen(filename, "r"); 
   char* token = readToken(fp);     
   int counter = 0;
   largest_weight = 1;
   while (token){
      if (counter < 2){
         counter++;
         if (atoi(token) > largest_vertex)
            largest_vertex = atoi(token);
      }
      else{
         counter = 0;
         if (token[0] != ';'){
            if (atoi(token) > largest_weight)
               largest_weight = atoi(token);
            token = readToken(fp);
         }
      }
      token = readToken(fp);
   }
   largest_vertex += 1;
   largest_weight += 1;
   fclose(fp);
}

static void updateVertexKey(binheap* b, Vertex* v, int weight){

   v->key = weight;
   v->owner = decreaseKeyBinHeap(b, v->owner, v);
}


void printMST(Vertex* starting_vertex){

   Queue* nodes = initializeQueue();
   PrintRecord**  print_list = calloc(largest_vertex, (sizeof(PrintRecord*)));

   int number_of_nodes = 0;

   int total_weight = 0;
   int depth = 0;
   int index = 0;
   int isfirst = true;
   enqueue(nodes, starting_vertex);
   while (true){

      number_of_nodes = nodes->number_of_elements;
      if (number_of_nodes <= 0)
         break;
      while (number_of_nodes > 0){

         Vertex* temp = newVertex(largest_weight);
         temp = nodes->bottom->value;

         if (isfirst){
            addList(print_list, &index, temp, adjacency_matrix, &total_weight);
            printList(print_list, index, depth);
            fprintf(fp1, ";\n");
            index = 0;
            isfirst = false;
            depth++;
         }
     dequeue(nodes);
         for (int i = 0; i < largest_vertex; i++){
            if (List_of_vertices[i] != NULL && adjacency_matrix[temp->vertex_value][i] != EMPTY  && List_of_vertices[i]->predecessor == temp->vertex_value){
               enqueue(nodes, List_of_vertices[i]);
               addList(print_list, &index, List_of_vertices[i], adjacency_matrix, &total_weight);

            }
         }
         number_of_nodes--;
      }
      number_of_nodes = nodes->number_of_elements;
      if (number_of_nodes <= 0)
         break;
      printList(print_list, index, depth);
      fprintf(fp1, ";\n");
      index = 0;
      number_of_nodes = nodes->number_of_elements;
      depth++;
   }
   fprintf(fp1, "weight: %d\n", total_weight);
   total_weight = 0;
}

void addList(PrintRecord** list, int *index, Vertex* v, int** adjacency_matrix, int* total_weight){

   PrintRecord* temp = calloc(1, sizeof(PrintRecord));

   if (v->predecessor == -1)
      temp->vertex = v->vertex_value;
   else{
      *total_weight += adjacency_matrix[v->vertex_value][v->predecessor];
      temp->vertex = v->vertex_value;
      temp->predecessor = v->predecessor;
      temp->edge_weight = adjacency_matrix[v->vertex_value][v->predecessor];
   }
   list[*index] = temp;
   *index += 1;
}

void printList(PrintRecord** list, int size, int depth){

   PrintRecord* value;
   //sorting adjacent vertices via insertion sort
   int j = 0;
   for (int i = 1; i < size; i++){
      value = list[i];
      j = i - 1;
      while (j >= 0 && list[j]->vertex > value->vertex){
         list[j + 1] = list[j];
         j = j - 1;
      }
      list[j + 1] = value;
   }

   int number_printed = 0;
   fprintf(fp1, "%d : ", depth);

   for (int i = 0; i < size; i++){
      if (list[i]->edge_weight == 0)
         fprintf(fp1, "%d", list[i]->vertex);
      else
         fprintf(fp1, "%d(%d)%d", list[i]->vertex, list[i]->predecessor, list[i]->edge_weight);
      if ((number_printed + 1) != size)
         fprintf(fp1, ", ");
      number_printed++;
   }
}

void printNode(Vertex* v, int** adjacency_matrix, int* total_weight){

   if (v->predecessor == -1)
      fprintf(fp1, "%d", v->vertex_value);
   
   else{
      *total_weight += adjacency_matrix[v->vertex_value][v->predecessor];
      fprintf(fp1, "%d(%d)%d", v->vertex_value, v->predecessor, adjacency_matrix[v->vertex_value][v->predecessor]);
   }
}